FROM openjdk:9

RUN mkdir /accounting
COPY build/libs/accounting.jar /accounting/accounting.jar

ENTRYPOINT ["java", "-jar", "/accounting/accounting.jar", "--spring.profiles.active={$PROFILE}"]
