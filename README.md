# Accounting

Welcome all. This projects aim is to be the GNU Cache as an self hosted version.

## How to run the project

First of all make the environment variables. In the `.env.dist` file are default values defined. All possible
environment variables are also in the `.env.dist` file defined.

Execute `cp .env.dist .env`
