package de.team.accounting.annotation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = NotBlankConstraintValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface NotBlankConstraint {

    /**
     * Default constraint validation error message.
     * @return Error message
     */
    String message() default "constraint.notBlank";

    /**
     * Default minimum length.
     *
     * @return Minimum length
     */
    int minLength() default  3;

    /**
     * Default maximum length.
     *
     * @return Maximum length
     */
    int maxLength() default 255;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
