package de.team.accounting.annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NotBlankConstraintValidator implements ConstraintValidator<NotBlankConstraint, String> {

    private int minLength;

    private int maxLength;

    @Override
    public void initialize(NotBlankConstraint constraintAnnotation) {
        minLength = constraintAnnotation.minLength();
        maxLength = constraintAnnotation.maxLength();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        if (value == null) {
            return false;
        }

        int trimmedValueLength = value.trim().length();

        return trimmedValueLength < maxLength && trimmedValueLength >= minLength;
    }
}
