package de.team.accounting.application.account.controller;

import de.team.accounting.application.account.presenter.AccountPresenter;
import de.team.accounting.application.account.service.AccountService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiAccountController {

    private AccountService service;

    public ApiAccountController(AccountService service) {
        this.service = service;
    }

    @GetMapping("/api/accounts")
    public ResponseEntity allAccounts(
        Authentication authentication,
        @PageableDefault(page = 0, size = 5)
        @SortDefault.SortDefaults({@SortDefault(sort = "name", direction = Sort.Direction.ASC)}) Pageable pageable
    ) {
        AccountPresenter presenter = new AccountPresenter();
        service.fetchAllUserAccounts(authentication, pageable, presenter);
        return presenter.get();
    }
}
