package de.team.accounting.application.account.form;

import de.team.accounting.annotation.NotBlankConstraint;
import lombok.Data;

@Data
public class NewAccountForm {

    @NotBlankConstraint
    private String name;

    @NotBlankConstraint
    private String category;
}
