package de.team.accounting.application.account.presenter;

import de.team.accounting.domain.account.dto.AccountOutputDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class AccountPresenter implements Supplier<ResponseEntity>, Consumer<AccountOutputDto> {

    private ResponseEntity result;

    private List<Consumer<AccountOutputDto>> consumerChain = new ArrayList<>();

    public AccountPresenter() {
        consumerChain.add(this::presentError);
        consumerChain.add(this::presentAccount);
        consumerChain.add(this::presentAccountPage);
    }

    @Override
    public ResponseEntity get() {
        return result;
    }

    @Override
    public void accept(AccountOutputDto accountOutputDto) {
        consumerChain.forEach((consumer -> consumer.accept(accountOutputDto)));
    }

    private void presentError(AccountOutputDto accountOutputDto) {
        if (accountOutputDto.getErrorResponse() != null) {
            result = new ResponseEntity<>(accountOutputDto.getErrorResponse(), HttpStatus.BAD_REQUEST);
        }
    }

    private void presentAccount(AccountOutputDto accountOutputDto) {
        if (accountOutputDto.getResponse() != null) {
            result = new ResponseEntity<>(accountOutputDto.getResponse(), HttpStatus.OK);
        }
    }

    private void presentAccountPage(AccountOutputDto accountOutputDto) {
        if (accountOutputDto.getResponsePage() != null) {
            result = new ResponseEntity<>(accountOutputDto.getResponsePage(), HttpStatus.OK);
        }
    }
}
