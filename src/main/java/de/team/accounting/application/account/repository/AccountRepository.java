package de.team.accounting.application.account.repository;

import de.team.accounting.domain.account.entity.Account;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends PagingAndSortingRepository<Account, String> {

    List<Account> findAllByUserId(String userId);

    Page<Account> findByUserId(String userId, Pageable pageable);
}
