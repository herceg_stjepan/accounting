package de.team.accounting.application.account.service;

import de.team.accounting.application.account.repository.AccountRepository;
import de.team.accounting.domain.UseCase;
import de.team.accounting.domain.account.dto.AccountInputDto;
import de.team.accounting.domain.account.dto.AccountOutputDto;
import de.team.accounting.security.model.User;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;
import java.util.function.Supplier;

@Service
public class AccountFacade implements AccountService {

    private AccountRepository accountRepository;

    private UseCase<AccountInputDto, AccountOutputDto> addNewAccountUseCase;

    private UseCase<AccountInputDto, AccountOutputDto> findUserAccountsUseCase;

    public AccountFacade(
        AccountRepository accountRepository,
        @Qualifier("addNewAccount") UseCase<AccountInputDto, AccountOutputDto> addNewAccountUseCase,
        @Qualifier("findAllUserAccounts") UseCase<AccountInputDto, AccountOutputDto> findUserAccountsUseCase
    ) {
        this.accountRepository = accountRepository;
        this.addNewAccountUseCase = addNewAccountUseCase;
        this.findUserAccountsUseCase = findUserAccountsUseCase;
    }

    @Override
    public void fetchAllUserAccounts(
        Authentication authentication,
        Pageable pageable,
        Consumer<AccountOutputDto> consumer
    ) {
        findUserAccountsUseCase.execute(
            () -> new AccountInputDto((User) authentication.getPrincipal(), pageable),
            consumer
        );
    }

    @Override
    public void addNewAccount(
        Supplier<AccountInputDto> supplier,
        Consumer<AccountOutputDto> consumer
    ) {
        addNewAccountUseCase.execute(supplier, consumer);
    }
}
