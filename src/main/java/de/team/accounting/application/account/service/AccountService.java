package de.team.accounting.application.account.service;

import de.team.accounting.domain.account.dto.AccountInputDto;
import de.team.accounting.domain.account.dto.AccountOutputDto;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;

import java.util.function.Consumer;
import java.util.function.Supplier;

public interface AccountService {

    void addNewAccount(Supplier<AccountInputDto> supplier, Consumer<AccountOutputDto> consumer);

    void fetchAllUserAccounts(
        Authentication authentication,
        Pageable pageable,
        Consumer<AccountOutputDto> consumer
    );
}
