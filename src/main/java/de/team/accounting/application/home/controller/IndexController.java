package de.team.accounting.application.home.controller;

import de.team.accounting.application.home.presenter.HomePresenter;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {

    /**
     * Returns index page.
     *
     * @return index html page
     */
    @GetMapping(value = "/")
    public ResponseEntity getIndex() {
        return new HomePresenter().get();
    }
}
