package de.team.accounting.application.home.presenter;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.function.Supplier;

public class HomePresenter implements Supplier<ResponseEntity> {
    @Override
    public ResponseEntity get() {
        return new ResponseEntity<>(new Response(), HttpStatus.OK);
    }

    @Data
    class Response {

        private String greeting;

        private Date date;

        Response() {
            greeting = "Welcome to accounting";
            date = new Date();
        }
    }
}
