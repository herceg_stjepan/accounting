package de.team.accounting.application.login.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import de.team.accounting.security.model.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Optional;

@Service
public class JwtLoginService {

    static final long EXPIRATION_TIME = 28800000; //8 hours

    static final String TOKEN_PREFIX = "Bearer";

    static final String HEADER_STRING = "Authorization";

    static final String COOKIE_KEY = "token";

    private final String signingSecret;

    private final String issuer;

    public JwtLoginService(
        @Value("${jwt.secret}") String signingSecret,
        @Value("${jwt.issuer}") String issuer
    ) {
        this.signingSecret = signingSecret;
        this.issuer = issuer;
    }

    public String createJwt(String username) throws UnsupportedEncodingException {
        return JWT.create()
            .withIssuer(issuer)
            .withSubject(username)
            .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
            .sign(Algorithm.HMAC256(signingSecret));
    }

    public String createJwt(Authentication authentication) throws UnsupportedEncodingException {
        User user = (User) authentication.getPrincipal();
        return JWT.create()
            .withIssuer(issuer)
            .withSubject(user.getEmail())
            .withClaim("userId", user.getId())
            .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
            .sign(Algorithm.HMAC256(signingSecret));
    }

    public void addAuthentication(
        HttpServletResponse response,
        Authentication auth
    ) throws UnsupportedEncodingException {
        response.addCookie(new Cookie(COOKIE_KEY, createJwt(auth)));
    }

    public Authentication getAuthentication(HttpServletRequest request) {

        if (request.getCookies() != null) {
            Optional<Cookie> tokenFromCookie = Arrays.stream(request.getCookies())
                .filter(c -> c.getName().equals(COOKIE_KEY)).findFirst();
            if (tokenFromCookie.isPresent()) {
                try {
                    JWTVerifier verifier = createVerifier();
                    DecodedJWT decodedJWT = verifier.verify(tokenFromCookie.get().getValue());
                    return createAuthToken(
                        decodedJWT.getSubject(),
                        decodedJWT.getClaim("userId").asString()
                    );
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage());
                }
            }
        }

        return null;
    }

    private UsernamePasswordAuthenticationToken createAuthToken(String username, String id) {
        User user = new User();
        user.setEmail(username);
        user.setId(id);
        return new UsernamePasswordAuthenticationToken(user, null, Collections.emptyList());
    }

    private JWTVerifier createVerifier() throws Exception {
        return JWT.require(Algorithm.HMAC256(signingSecret))
            .withIssuer(issuer)
            .build();
    }
}