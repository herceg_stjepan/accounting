package de.team.accounting.application.profile.controller;

import de.team.accounting.application.profile.form.UpdateProfileForm;
import de.team.accounting.application.profile.presenter.ProfilePresenter;
import de.team.accounting.application.profile.service.ProfileFacade;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ApiProfileController {

    private ProfileFacade profileFacade;

    public ApiProfileController(ProfileFacade profileFacade) {
        this.profileFacade = profileFacade;
    }

    @GetMapping("/api/profile/{userName}")
    public ResponseEntity showProfile(@PathVariable("userName") String username) {
        ProfilePresenter presenter = new ProfilePresenter();
        profileFacade.showProfileForUserName(username, presenter);
        return presenter.get();
    }

    @PutMapping("/api/profile/{userName}")
    public ResponseEntity updateProfile(
        @RequestBody @Valid UpdateProfileForm updateProfileForm,
        Authentication authentication
    ) {
        ProfilePresenter presenter = new ProfilePresenter();
        profileFacade.updateProfile(updateProfileForm, authentication.getName(), presenter);
        return presenter.get();
    }
}
