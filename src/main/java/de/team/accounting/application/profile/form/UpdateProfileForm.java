package de.team.accounting.application.profile.form;

import de.team.accounting.annotation.NotBlankConstraint;
import lombok.Data;

@Data
public class UpdateProfileForm {

    @NotBlankConstraint
    private String firstName;

    @NotBlankConstraint
    private String lastName;

    @NotBlankConstraint
    private String oldPassword;

    @NotBlankConstraint
    private String newPassword;
}
