package de.team.accounting.application.profile.presenter;

import de.team.accounting.domain.profile.dto.ProfileOutputDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class ProfilePresenter implements Supplier<ResponseEntity>, Consumer<ProfileOutputDto> {

    private ResponseEntity responseEntity;

    private List<Consumer<ProfileOutputDto>> consumerChain;

    public ProfilePresenter() {
        responseEntity = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        consumerChain = new ArrayList<>();
        consumerChain.add(this::onResponse);
        consumerChain.add(this::onError);
    }

    @Override
    public void accept(ProfileOutputDto profileOutputDto) {
        consumerChain.forEach((consumer) -> consumer.accept(profileOutputDto));
    }

    @Override
    public ResponseEntity get() {
        return responseEntity;
    }

    private void onResponse(ProfileOutputDto profileOutputDto) {
        if (profileOutputDto.getResult() != null) {
            responseEntity = new ResponseEntity<>(profileOutputDto.getResult(), HttpStatus.OK);
        }
    }

    private void onError(ProfileOutputDto profileOutputDto) {
        if (profileOutputDto.getError() != null) {
            this.responseEntity = new ResponseEntity<>(
                profileOutputDto.getError(),
                HttpStatus.BAD_REQUEST
            );
        }
    }
}
