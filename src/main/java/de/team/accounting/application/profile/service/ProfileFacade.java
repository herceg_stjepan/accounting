package de.team.accounting.application.profile.service;

import de.team.accounting.application.profile.form.UpdateProfileForm;
import de.team.accounting.domain.UseCase;
import de.team.accounting.domain.profile.dto.ProfileInputDto;
import de.team.accounting.domain.profile.dto.ProfileOutputDto;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;

@Service
public class ProfileFacade {

    UseCase<ProfileInputDto, ProfileOutputDto> showProfileData;
    UseCase<ProfileInputDto, ProfileOutputDto> updateProfile;

    public ProfileFacade(
        @Qualifier("showProfileUseCase") UseCase<ProfileInputDto, ProfileOutputDto> showProfileData,
        @Qualifier("updateProfileUseCase") UseCase<ProfileInputDto, ProfileOutputDto> updateProfile
    ) {
        this.showProfileData = showProfileData;
        this.updateProfile = updateProfile;
    }

    public void showProfileForUserName(String username, Consumer<ProfileOutputDto> consumer) {
        showProfileData.execute(() -> new ProfileInputDto(username), consumer);
    }

    public void updateProfile(UpdateProfileForm updateProfileFormForm, String username, Consumer<ProfileOutputDto> consumer) {
        updateProfile.execute(
            () -> {
                ProfileInputDto profileInputDto = new ProfileInputDto(updateProfileFormForm);
                profileInputDto.setUsername(username);
                return profileInputDto;
            },
            consumer
        );
    }
}
