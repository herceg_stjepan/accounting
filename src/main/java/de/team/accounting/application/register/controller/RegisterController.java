package de.team.accounting.application.register.controller;

import de.team.accounting.application.register.forms.UserForm;
import de.team.accounting.application.register.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class RegisterController {

    @Autowired
    private RegisterService registerService;

    @GetMapping(value = "/register")
    public ModelAndView getRegisterPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("register/register");
        modelAndView.addObject("userForm", new UserForm());
        return modelAndView;
    }

    @PostMapping(value = "/register")
    public ModelAndView saveUser(@Valid UserForm userForm, BindingResult bindingResult) {

        ModelAndView modelAndView = new ModelAndView();

        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("register/register");
            return modelAndView;
        }

        try {
            registerService.createUser(userForm);
            modelAndView.setViewName("redirect:/login");
            return modelAndView;
        } catch (Exception exception) {
            modelAndView.setViewName("redirect:/login");
            return modelAndView;
        }
    }
}
