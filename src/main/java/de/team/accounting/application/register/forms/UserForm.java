package de.team.accounting.application.register.forms;

import de.team.accounting.annotation.NotBlankConstraint;
import lombok.Data;

import javax.validation.constraints.Email;

@Data
public class UserForm {

    @Email
    @NotBlankConstraint
    private String email;

    @NotBlankConstraint
    private String firstName;

    @NotBlankConstraint
    private String lastName;

    @NotBlankConstraint
    private String password;

    @NotBlankConstraint
    private String repeatPassword;
}
