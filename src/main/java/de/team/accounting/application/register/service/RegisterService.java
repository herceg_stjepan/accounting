package de.team.accounting.application.register.service;

import de.team.accounting.application.register.forms.UserForm;
import de.team.accounting.security.model.User;
import de.team.accounting.security.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class RegisterService {

    @Autowired
    private UsersRepository userRepository;

    @Autowired
    @Qualifier("bCrypt")
    private PasswordEncoder bCrypt;

    public void createUser(UserForm userForm) {
        User user = new User();
        user.setEmail(userForm.getEmail());
        user.setFirstName(userForm.getFirstName());
        user.setLastName(userForm.getLastName());
        user.setPassword(bCrypt.encode(userForm.getPassword()));
        user.setCreatedAt(new Date());
        userRepository.save(user);
    }

    public Iterable<User> findAllUsers() {
        return userRepository.findAll();
    }
}
