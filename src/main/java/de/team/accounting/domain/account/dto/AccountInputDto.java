package de.team.accounting.domain.account.dto;

import de.team.accounting.application.account.form.NewAccountForm;
import de.team.accounting.security.model.User;
import lombok.Data;
import org.springframework.data.domain.Pageable;

@Data
public class AccountInputDto {

    private String accountName;

    private String category;

    private User user;

    private Pageable pageable;

    public AccountInputDto(NewAccountForm newAccountForm, User user) {
        this.accountName = newAccountForm.getName();
        this.category = newAccountForm.getCategory();
        this.user = user;
    }

    public AccountInputDto(User user, Pageable pageable) {
        this.user = user;
        this.pageable = pageable;
    }
}
