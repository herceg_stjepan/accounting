package de.team.accounting.domain.account.dto;

import de.team.accounting.domain.account.entity.Account;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.Date;

@Data
public class AccountOutputDto {

    private ResponseData response;

    private Page<ResponseData> responsePage;

    private ErrorResponse errorResponse;

    public AccountOutputDto(Account account) {
        response = new ResponseData(account);
    }

    public AccountOutputDto(Throwable throwable) {
        errorResponse = new ErrorResponse(throwable);
    }

    public AccountOutputDto(Page<Account> accountPage) {
        responsePage = accountPage.map(ResponseData::new);
    }

    @Data
    class ResponseData {

        private String accountId;

        private String accountName;

        private String accountCategory;

        public ResponseData(String accountId, String accountName, String accountCategory) {
            this.accountId = accountId;
            this.accountName = accountName;
            this.accountCategory = accountCategory;
        }

        public ResponseData(Account account) {
            accountId = account.getId();
            accountCategory = account.getCategory();
            accountName = account.getName();
        }
    }

    @Data
    class ErrorResponse {

        private String message;

        private Date date;

        ErrorResponse(Throwable throwable) {
            message = throwable.getMessage();
            date = new Date();
        }
    }
}
