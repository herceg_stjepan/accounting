package de.team.accounting.domain.account.entity;

import de.team.accounting.annotation.NotBlankConstraint;
import de.team.accounting.domain.account.dto.AccountInputDto;
import de.team.accounting.security.model.User;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity(name = "accounts")
public class Account {

    @Id
    @Column(name = "account_id")
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @NotBlankConstraint
    private String id;

    @NotBlankConstraint
    private String name;

    @NotBlankConstraint
    private String category;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    public Account(AccountInputDto inputDto) {
        name = inputDto.getAccountName();
        category = inputDto.getCategory();
        user = inputDto.getUser();
    }
}
