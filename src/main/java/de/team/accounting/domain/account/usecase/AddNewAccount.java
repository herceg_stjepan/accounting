package de.team.accounting.domain.account.usecase;

import de.team.accounting.domain.account.entity.Account;
import de.team.accounting.application.account.repository.AccountRepository;
import de.team.accounting.domain.UseCase;
import de.team.accounting.domain.account.dto.AccountInputDto;
import de.team.accounting.domain.account.dto.AccountOutputDto;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;
import java.util.function.Supplier;

@Component
public class AddNewAccount extends BaseAccountUseCase implements UseCase<AccountInputDto, AccountOutputDto> {

    public AddNewAccount(AccountRepository repository) {
        super(repository);
    }

    @Override
    public void execute(Supplier<AccountInputDto> input, Consumer<AccountOutputDto> output) {
        try {
            output.accept(new AccountOutputDto(repository.save(new Account(input.get()))));
        } catch (Exception e) {
            output.accept(new AccountOutputDto(e));
        }
    }
}
