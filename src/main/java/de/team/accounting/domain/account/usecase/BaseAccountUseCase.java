package de.team.accounting.domain.account.usecase;

import de.team.accounting.application.account.repository.AccountRepository;
import lombok.Data;

@Data
abstract class BaseAccountUseCase {

    protected AccountRepository repository;

    public BaseAccountUseCase(AccountRepository repository) {
        this.repository = repository;
    }
}
