package de.team.accounting.domain.account.usecase;

import de.team.accounting.application.account.repository.AccountRepository;
import de.team.accounting.domain.UseCase;
import de.team.accounting.domain.account.dto.AccountInputDto;
import de.team.accounting.domain.account.dto.AccountOutputDto;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;
import java.util.function.Supplier;

@Component
public class FindAllUserAccounts extends BaseAccountUseCase implements UseCase<AccountInputDto, AccountOutputDto> {

    public FindAllUserAccounts(AccountRepository repository) {
        super(repository);
    }

    @Override
    public void execute(Supplier<AccountInputDto> input, Consumer<AccountOutputDto> output) {
        try {
            AccountInputDto inputDto = input.get();
            output.accept(
                new AccountOutputDto(
                    repository.findByUserId(
                        inputDto.getUser().getId(),
                        inputDto.getPageable()
                    )
                )
            );
        } catch (Exception e) {
            output.accept(new AccountOutputDto(e));
        }
    }
}
