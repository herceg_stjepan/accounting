package de.team.accounting.domain.profile.dto;

import de.team.accounting.application.profile.form.UpdateProfileForm;
import lombok.Data;

@Data
public class ProfileInputDto {

    private String username;

    private String oldPassword;

    private String newPassword;

    private String firstName;

    private String lastName;

    public ProfileInputDto(String username) {
        this.username = username;
    }

    public ProfileInputDto(UpdateProfileForm updateProfileFormForm) {
        oldPassword = updateProfileFormForm.getOldPassword();
        newPassword = updateProfileFormForm.getNewPassword();
        firstName = updateProfileFormForm.getFirstName();
        lastName = updateProfileFormForm.getLastName();
    }
}
