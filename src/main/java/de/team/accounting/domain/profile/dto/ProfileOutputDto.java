package de.team.accounting.domain.profile.dto;

import de.team.accounting.security.model.User;
import lombok.Data;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Data
public class ProfileOutputDto {

    private Response result;

    private Throwable error;

    public ProfileOutputDto(User user) {
        this.result = new Response(user);
    }

    public ProfileOutputDto(UsernameNotFoundException e) {
        this.error = e;
    }

    public ProfileOutputDto(Exception e) {
        error = e;
    }

    @Data
    class Response {

        private String id;

        private String userName;

        private String firstName;

        private String lastName;

        private String email;

        Response(User user) {
            id = user.getId();
            userName = user.getEmail();
            email = user.getEmail();
            firstName = user.getFirstName();
            lastName = user.getLastName();
        }
    }
}
