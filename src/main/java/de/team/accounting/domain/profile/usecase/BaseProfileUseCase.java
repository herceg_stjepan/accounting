package de.team.accounting.domain.profile.usecase;

import de.team.accounting.security.repository.UsersRepository;

abstract public class BaseProfileUseCase {

    protected UsersRepository usersRepository;

    public BaseProfileUseCase(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }
}
