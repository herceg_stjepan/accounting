package de.team.accounting.domain.profile.usecase;

import de.team.accounting.domain.UseCase;
import de.team.accounting.domain.profile.dto.ProfileInputDto;
import de.team.accounting.domain.profile.dto.ProfileOutputDto;
import de.team.accounting.security.model.User;
import de.team.accounting.security.repository.UsersRepository;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

@Component
public class ShowProfileUseCase extends BaseProfileUseCase implements UseCase<ProfileInputDto, ProfileOutputDto> {

    public ShowProfileUseCase(UsersRepository usersRepository) {
        super(usersRepository);
    }

    @Override
    public void execute(Supplier<ProfileInputDto> input, Consumer<ProfileOutputDto> output) {
        ProfileInputDto inputDto = input.get();
        Optional<User> user = usersRepository.findByEmail(inputDto.getUsername());

        user.ifPresentOrElse(
            user1 -> output.accept(new ProfileOutputDto(user1)),
            () -> output.accept(
                new ProfileOutputDto(
                    new UsernameNotFoundException(
                        "User data for " + inputDto.getUsername() + " could not be found"
                    )
                )
            )
        );
    }
}
