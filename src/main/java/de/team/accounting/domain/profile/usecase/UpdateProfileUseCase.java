package de.team.accounting.domain.profile.usecase;

import de.team.accounting.domain.UseCase;
import de.team.accounting.domain.profile.dto.ProfileInputDto;
import de.team.accounting.domain.profile.dto.ProfileOutputDto;
import de.team.accounting.security.model.User;
import de.team.accounting.security.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Supplier;

@Component
public class UpdateProfileUseCase extends BaseProfileUseCase implements UseCase<ProfileInputDto, ProfileOutputDto> {

    private PasswordEncoder encoder;

    public UpdateProfileUseCase(
        UsersRepository usersRepository,
        @Qualifier("bCrypt") PasswordEncoder encoder
    ) {
        super(usersRepository);
        this.encoder = encoder;
    }

    @Override
    public void execute(Supplier<ProfileInputDto> input, Consumer<ProfileOutputDto> output) {
        ProfileInputDto inputDto = input.get();
        Optional<User> optionalUser = usersRepository.findByEmail(inputDto.getUsername());

        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            user.setFirstName(inputDto.getFirstName());
            user.setLastName(inputDto.getLastName());
            if (BCrypt.checkpw(inputDto.getOldPassword(), user.getPassword())) {
                user.setPassword(encoder.encode(inputDto.getNewPassword()));
            } else {
                output.accept(new ProfileOutputDto(
                    new RuntimeException("Password does not match")
                ));
                return;
            }
            try {
                usersRepository.save(user);
                output.accept(new ProfileOutputDto(user));
            } catch (Exception e) {
                output.accept(new ProfileOutputDto(e));
            }
        }
    }
}
