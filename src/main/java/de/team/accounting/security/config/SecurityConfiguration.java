package de.team.accounting.security.config;

import de.team.accounting.application.login.service.JwtLoginService;
import de.team.accounting.security.filter.JwtAuthenticationFilter;
import de.team.accounting.security.filter.JwtAuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    @Qualifier("bCrypt")
    private PasswordEncoder bCrypt;

    @Autowired
    private JwtLoginService jwtLoginService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.cors().disable();
        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.authorizeRequests()
            .antMatchers("/").permitAll()
            .antMatchers("/jquery/**").permitAll()
            .antMatchers("/bootstrap/**").permitAll()
            .antMatchers("/api/**").fullyAuthenticated()
            .anyRequest().authenticated();

        http.addFilter(new JwtAuthenticationFilter(authenticationManager(), jwtLoginService));
        http.addFilter(new JwtAuthorizationFilter(authenticationManager(), jwtLoginService));
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService)
            .passwordEncoder(bCrypt);
    }
}
