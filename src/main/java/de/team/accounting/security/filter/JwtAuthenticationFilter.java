package de.team.accounting.security.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.team.accounting.application.login.service.JwtLoginService;
import de.team.accounting.security.credentials.AccountCredentials;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private JwtLoginService jwtLoginService;

    public JwtAuthenticationFilter(
        AuthenticationManager manager,
        JwtLoginService jwtLoginService
    ) {
        setAuthenticationManager(manager);
        this.jwtLoginService = jwtLoginService;
    }

    @Override
    public Authentication attemptAuthentication(
        HttpServletRequest request,
        HttpServletResponse response
    ) throws AuthenticationException {

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            AccountCredentials creds = objectMapper
                .readValue(request.getInputStream(), AccountCredentials.class);
            return getAuthenticationManager().authenticate(
                new UsernamePasswordAuthenticationToken(
                    creds.getUsername(),
                    creds.getPassword(),
                    Collections.emptyList()
                )
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(
        HttpServletRequest req,
        HttpServletResponse res,
        FilterChain chain,
        Authentication auth
    ) throws IOException, ServletException {
        jwtLoginService.addAuthentication(res, auth);
    }
}
