package de.team.accounting.security.filter;

import de.team.accounting.application.login.service.JwtLoginService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter {

    private JwtLoginService jwtLoginService;

    public JwtAuthorizationFilter(AuthenticationManager manager, JwtLoginService jwtLoginService) {
        super(manager);
        this.jwtLoginService = jwtLoginService;
    }

    @Override
    protected void doFilterInternal(
        HttpServletRequest request,
        HttpServletResponse response,
        FilterChain chain
    ) throws IOException, ServletException {

        Authentication authentication = jwtLoginService
            .getAuthentication(request);

        SecurityContextHolder.getContext()
            .setAuthentication(authentication);

        chain.doFilter(request, response);
    }
}
