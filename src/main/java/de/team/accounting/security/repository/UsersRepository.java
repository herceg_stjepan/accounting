package de.team.accounting.security.repository;

import de.team.accounting.security.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsersRepository extends CrudRepository<User, String> {

    /**
     * Returns user for given email address.
     *
     * @param email Email
     * @return User
     */
    Optional<User> findByEmail(String email);
}
