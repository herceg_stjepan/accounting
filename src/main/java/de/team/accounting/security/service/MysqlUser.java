package de.team.accounting.security.service;

import de.team.accounting.security.model.User;
import de.team.accounting.security.repository.UsersRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MysqlUser implements UserDetailsService {

    private UsersRepository usersRepository;

    public MysqlUser(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> optionalUser = usersRepository.findByEmail(username);
        return optionalUser
            .orElseThrow(() -> new UsernameNotFoundException("Username not found"));
    }
}
