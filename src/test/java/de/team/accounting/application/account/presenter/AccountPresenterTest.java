package de.team.accounting.application.account.presenter;

import de.team.accounting.domain.account.dto.AccountOutputDto;
import de.team.accounting.domain.account.entity.Account;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AccountPresenterTest {

    private AccountPresenter accountPresenter;

    @Before
    public void setUp() {
        accountPresenter = new AccountPresenter();
    }

    @Test
    public void accept_handlesError_ReturnsBadRequest() {

        AccountOutputDto outputDto = new AccountOutputDto(new Exception("Some exception"));

        accountPresenter.accept(outputDto);

        assertEquals(HttpStatus.BAD_REQUEST, accountPresenter.get().getStatusCode());
        assertEquals(outputDto.getErrorResponse(), accountPresenter.get().getBody());
    }

    @Test
    public void accept_handlesOneAccount_ReturnsOk() {

        AccountOutputDto outputDto = new AccountOutputDto(new Account());

        accountPresenter.accept(outputDto);

        assertEquals(HttpStatus.OK, accountPresenter.get().getStatusCode());
        assertEquals(outputDto.getResponse(), accountPresenter.get().getBody());
    }

    @Test
    public void accept_handlesPageOfAccount_ReturnsOk() {

        List<Account> accounts = new ArrayList<>();
        accounts.add(new Account());
        accounts.add(new Account());
        accounts.add(new Account());

        AccountOutputDto outputDto = new AccountOutputDto(new PageImpl<>(accounts));

        accountPresenter.accept(outputDto);

        assertEquals(HttpStatus.OK, accountPresenter.get().getStatusCode());
    }
}
