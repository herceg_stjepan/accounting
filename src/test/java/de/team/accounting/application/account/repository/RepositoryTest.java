package de.team.accounting.application.account.repository;

import de.team.accounting.domain.account.entity.Account;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RepositoryTest {

    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void findById_wrongId_isPresentIsFalse() {
        Optional<Account> optionalAccount = accountRepository.findById("hallo");
        assertFalse(optionalAccount.isPresent());
    }

    @Test
    public void findById_wrongId_isPresentIsTrue() {
        Optional<Account> optionalAccount = accountRepository.findById("7ad094ca-10ca-421b-a9f5-8eefad5564d4");
        assertTrue(optionalAccount.isPresent());
    }
}
