package de.team.accounting.application.home.presenter;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import static org.junit.Assert.assertEquals;

public class HomePresenterTest {

    private HomePresenter homePresenter;

    @Before
    public void setUp() {
        homePresenter = new HomePresenter();
    }

    @Test
    public void get_handlesIndex_returnsResponseOk() {
        assertEquals(HttpStatus.OK, homePresenter.get().getStatusCode());
        assertEquals("Welcome to accounting", ((HomePresenter.Response)homePresenter.get().getBody()).getGreeting());
    }
}
