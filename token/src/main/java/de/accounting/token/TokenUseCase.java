package de.accounting.token;

public interface TokenUseCase<P, R> {

    interface Callback<R> {

        void success(R output);

        void error(Throwable throwable);
    }

    void execute(P input, Callback<R> callback);
}
